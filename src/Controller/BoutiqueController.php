<?php

namespace App\Controller;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Boutique;

class BoutiqueController  extends AbstractController
{
    /**
     *
     * @Route("/api/sezane/boutiques", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Retourne une liste de boutiques en fonction des paramètres transmis"
     * )
     * @SWG\Parameter(
     *     name="order",
     *     in="query",
     *     type="string",
     *     description="The field used to order rewards"
     * )
     */
    public function cgetBoutiques()
    {
    	$boutiques = $this->getDoctrine()
	        ->getRepository(Boutique::class)
	        ->findAll();

        return $this->json($boutiques, 200);
    }

    /**
     *
     * @Route("/api/sezane/boutiques/{id}", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Retourne la boutique {id}"
     * )
     */
    public function getBoutiques($id)
    {
    	$boutique = $this->getDoctrine()
	        ->getRepository(Boutique::class)
	        ->find($id);

		if (!$boutique) {
	        $this->json($boutique, 'No product found for id '.$id, 404);
	    }

        return $this->json($boutique, 200);
    }

    /**
     *
     * @Route("/api/sezane/boutiques", methods={"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="Retourne la boutique créée"
     * )
     */
    public function postBoutiques()
    {
    	$entityManager = $this->getDoctrine()->getManager();

    	$boutique = new Boutique();
/*
    	$boutique->setNom($nom)
    		->setLatitude($latitude)
    		->setLongitude($longitude)
    		->setAdresse($adresse);
*/
		$boutique->setNom("Boutique " . rand (1, 255))
    		->setLatitude(rand (1, 255))
    		->setLongitude(rand (1, 255));
    		
    	$entityManager->persist($product);
        $entityManager->flush();

        return $this->json($boutique, 200);
    }
}


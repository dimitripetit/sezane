<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoutiqueRepository")
 */
class Boutique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $latitude;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	private $createdAt;

	/**
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	private $updatedAt;

    /**
     *
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     *
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     *
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     *
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     *
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     *
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     *
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     *
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
	    return $this->createdAt;
	}

	/**
	 * @return \DateTime
	 */
	public function getUpdatedAt()
	{
	    return $this->updatedAt;
	}
}
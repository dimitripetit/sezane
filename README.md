## Récupérer le projet ##
git clone https://gitlab.com/dimitripetit/sezane.git

## Installer le projet ##
composer install

## Définir les paramètres de connection à la base de données MYSQL dans le fichier .env ##
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/sezane

## Installer les assets ##
php bin/console asset:install

## Création de la base de donnée ##
php bin/console doctrine:database:create

## Création du schéma de la base à partir des entitées ##
php bin/console doctrine:schema:create


## Accès à la documentation de l'API (NelmioApiDocBundle) ##
http://localhost/api/doc 

TODO - NE FONCTIONNE PAS problème de configuration avec Symfony 4

## Accès API

### Liste des boutiques ###
GET /api/sezane/boutiques 

### Liste de la boutique définie {id} ###
GET /api/sezane/boutiques/{$id} 

### Création d'un boutique ###
POST /apis/sezane/boutiques